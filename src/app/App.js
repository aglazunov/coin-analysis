import React, {Component} from 'react';
import {Route} from 'react-router';
import {Link} from 'react-router-dom';
import '../App.css';
import ConnectedLiquidityAnalysis from '../components/LiquidityAnalysis/ConnectedLiquidityAnalysis';
import ConnectedMarketOverview from '../components/MarketOverview/ConnectedMarketOverview';
import ConnectedCoinDetails from '../components/CoinDetails/ConnectedCoinDetails';
import * as Status from "./statuses";
import {Menu, Select} from 'semantic-ui-react';

class App extends Component {
    componentDidMount() {
        const {
            topCount,
            fetchTopCoins
        } = this.props;

        fetchTopCoins(topCount);
    }

    handleSelectChange = (event, data) => {
        this.props.changeTopCount(data.value);
    };

    render() {
        const {
            status,
            topCount
        } = this.props;

        const selectOptions = [
            {key: 10, value: 10, text: '10'},
            {key: 50, value: 50, text: '50'},
            {key: -1, value: -1, text: '100'}
        ];

        const menuItems = [
            {name: 'marketOverview', link: '/'},
            {name: 'liquidityAnalysis', link: '/liquidity'},
        ];

        const currentLocation = this.props.location.pathname;

        return (
            <div className="app">
                <Menu>
                    {menuItems.map(item => (<Menu.Item
                            key={item.name}
                            active={currentLocation === item.link}
                            as={Link}
                            name={item.name}
                            to={item.link}
                    />))}
                </Menu>

                <div className="content">

                    <div className="top-coins-selector">
                        <span>Top coins to show:</span>
                        <Select options={selectOptions} value={topCount} onChange={this.handleSelectChange}/>
                    </div>

                    {(status.status === Status.LOADING) && <div>{status.message}</div>}
                    {(status.status === Status.FAILURE) && <div>Failure: {status.message}</div>}
                    {(status.status === Status.SUCCESS) && (<>

                    <Route exact path="/" component={ConnectedMarketOverview} coins={this.props.coins}/>
                    <Route path="/liquidity" component={ConnectedLiquidityAnalysis} coins={this.props.coins}/>
                    <Route path="/coin/:id?" component={ConnectedCoinDetails}/>

                    </>)}
                </div>
            </div>
        );
    }
}

export default App;