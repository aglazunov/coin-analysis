import {withRouter} from 'react-router';
import * as actions from '../actions/actionCreators';
import {connect} from 'react-redux';
import App from './App';

const mapStateToProps = (state) => ({
    topCount: state.topCount,
    coins: state.coins,
    status: state.status
});

export default withRouter(connect(mapStateToProps, actions)(App));
