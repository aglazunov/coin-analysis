import {createStore, applyMiddleware} from 'redux';
import reducer from '../reducers/rootReducer';
import createLogger from 'redux-logger';

const addPromiseSupportToDispatch = store => next => action => {
    if (typeof action.then === 'function') {
        return action
            .then(next)
            .catch(next);
    }
    return next(action);
};

const addBatchSupportToDispatch = store => next => action => {
    if (action instanceof Array) {
        return action.map(action => next(action));
    }
    return next(action);
};

const configureStore = () => {
    const middlewares = [
        addBatchSupportToDispatch,
        addPromiseSupportToDispatch,
    ];
    if (process.env.NODE_ENV === 'development') {
        middlewares.push(createLogger);
    }
    return createStore(
        reducer,
        applyMiddleware(...middlewares)
    );
};

export default configureStore;