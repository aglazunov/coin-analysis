import React from 'react';
import PropTypes from 'prop-types'
import ConnectedApp from './ConnectedApp';
import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom'

const Root = ({store}) => (
    <Provider store={store}>
        <BrowserRouter>
            <ConnectedApp/>
        </BrowserRouter>
    </Provider>
);

Root.propTypes = {
    store: PropTypes.object.isRequired
};

export default Root;