const proxy = require('http-proxy-middleware');

module.exports = function(app) {
    app.use(proxy('/api', {
        target: 'https://api.coinmarketcap.com/v2/ticker/',
        pathRewrite: {
            '^/api/' : '/',     // rewrite path
        },
        changeOrigin: true,
        secure: false
    }));
};