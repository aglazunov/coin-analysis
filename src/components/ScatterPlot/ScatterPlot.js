// non-react component
// based on https://bl.ocks.org/mbostock/3887118

import d3Tip from 'd3-tip';
import * as d3 from 'd3';
import ResizeSensor from 'resize-sensor';
import markup from './markup';

const ScatterPlot = {
    init: function (element, options) {
        this.element = element;
        this.options = options;

        this.populateElement();
        new ResizeSensor(this.element, this.handleSizeUpdate.bind(this));

        // deferring update for Safari
        setTimeout(this.handleSizeUpdate.bind(this), 100);
    },

    populateElement: function () {
        const d3Element = d3.select(this.element),
            {xLabel, yLabel, tickFormat, margins} = this.options;

        d3Element.html(markup);

        this.margin = margins;
        this.color = d3.scaleLinear()
            .interpolate(d3.interpolateHcl)
            .range([d3.rgb("#ff0000"), d3.rgb('#00ff00')]);
        this.svg = d3Element.select('svg');
        this.content = this.svg.select('.content');
        this.dots = this.svg.select('.dots');
        this.xAxisElement = d3Element.select('.x.axis');
        this.yAxisElement = d3Element.select('.y.axis');
        this.x = d3.scaleLog().base(Math.E);
        this.y = d3.scaleLog().base(Math.E);
        this.z = d3.scaleLinear();

        this.tip = d3Tip()
            .attr('class', 'd3-tip')
            .offset([-10, 0])
            .html(this.options.tooltip);

        this.svg.call(this.tip);

        this.xAxis = d3.axisBottom();
        this.yAxis = d3.axisLeft();

        if (tickFormat) {
            this.xAxis.tickFormat(tickFormat);
            this.yAxis.tickFormat(tickFormat);
        }

        this.xAxisElement.select('text.label')
            .attr('y', -6)
            .style('text-anchor', 'end')
            .text(xLabel);

        this.yAxisElement
            .select('text.label')
            .attr('transform', 'rotate(-90)')
            .attr('y', 6)
            .attr('x', -6)
            .attr('dy', '.71em')
            .style('text-anchor', 'end')
            .text(yLabel);

        this.updateSizeBasedVariables();
    },

    update: function (data) {
        const {xProperty, yProperty, zProperty, colorProperty, radius = 3.5} = this.options;
        const maxRadius = 20;

        const propertyExtent = property => d3.extent(data, d => d[property]);

        this.x.domain(propertyExtent(xProperty)).nice();
        this.y.domain(propertyExtent(yProperty)).nice();
        this.z.domain(propertyExtent(zProperty))
            .range([radius, maxRadius]);
        this.color.domain(propertyExtent(colorProperty));

        let dots = this.dots.selectAll('.dot')
            .data(data);

        const dotsEnter = dots
            .enter().append('circle')
            .attr('class', 'dot')
            .on('mouseover', this.tip.show)
            .on('mouseout', this.tip.hide);

        dots.exit().remove();
        dots = dots.merge(dotsEnter);

        dots
            .attr('r', d => this.z(d[zProperty]))
            .attr('cx', d => this.x(d[xProperty]))
            .attr('cy', d => this.y(d[yProperty]))
            .style('fill', d => this.color(d[colorProperty]));
    },

    updateSizeBasedVariables: function () {
        this.width = this.element.clientWidth - this.margin.left - this.margin.right;
        this.height = this.element.clientHeight - this.margin.top - this.margin.bottom;

        this.x.range([0, this.width]);
        this.y.range([this.height, 0]);

        this.xAxis.scale(this.x)
            .tickSize(-(this.height), 0);
        this.yAxis.scale(this.y)
            .tickSize(-(this.width), 0);

        this.svg
            .attr('width', this.width + this.margin.left + this.margin.right)
            .attr('height', this.height + this.margin.top + this.margin.bottom);
        this.content
            .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

        this.xAxisElement
            .attr('transform', 'translate(0,' + this.height + ')')
            .call(this.xAxis);

        this.xAxisElement.select('text.label')
            .attr('x', this.width - 4);

        this.yAxisElement
            .call(this.yAxis);

        const {xProperty, yProperty} = this.options;

        this.dots.selectAll('.dot')
            .attr('cx', d => this.x(d[xProperty]))
            .attr('cy', d => this.y(d[yProperty]));
    },

    handleSizeUpdate: function () {
        this.updateSizeBasedVariables();
    }
};

export default ScatterPlot;