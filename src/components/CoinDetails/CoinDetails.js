import React from 'react';
import {Table, Header} from 'semantic-ui-react';

class CoinDetails extends React.Component {
    getCoinId() {
        return this.props.match.params.id;
    }

    componentDidMount() {
        this.props.fetchCoin(this.getCoinId());
    }

    render() {
        const coin = this.props.coin,
            upcaseFirst = str => str.substr(0, 1).toUpperCase() + str.substr(1),
            fields = ['name', 'symbol', 'rank', 'circulating_supply', 'total_supply'].map(field => ({
                id: field,
                label: upcaseFirst(field.split('_').join(' '))
            }));

        return <div>
            <Header size='large'>
                Coin Details
            </Header>
            {!coin && 'No coin available for display'}

            {coin && <Table stackable selectable>
                <Table.Header>
                    <Table.Row>
                        {fields.map(field => <Table.HeaderCell key={field.id}>{field.label}</Table.HeaderCell>)}
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    <Table.Row>
                        {fields.map(field => (
                            <Table.Cell key={field.id}>
                                {coin[field.id]}
                            </Table.Cell>
                        ))}
                    </Table.Row>
                </Table.Body>
            </Table>}
        </div>;
    }
}

export default CoinDetails;