import {connect} from 'react-redux';
import CoinDetails from './CoinDetails';
import {fetchCoin} from '../../actions/actionCreators';
import {getCoin} from '../../reducers/rootReducer';

const mapStateToProps = state => ({
    coin: getCoin(state)
});

export default connect(mapStateToProps, {
    fetchCoin
})(CoinDetails);