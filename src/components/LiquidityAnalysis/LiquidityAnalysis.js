import React, {Component} from 'react';
import styles from './LiquidityAnalysis.module.css';
import ScatterPlot from '../ScatterPlot/ScatterPlot';
import {Header} from 'semantic-ui-react';
import {bigValueFormat} from '../../util/chartUtils';
import './chart.css';

class LiquidityAnalysis extends Component {
    constructor(props) {
        super(props);
        this.chart = React.createRef();
    }

    componentDidMount() {
        const chartOptions = {
            xProperty: 'marketCap',
            yProperty: 'volume',
            zProperty: 'delta',
            colorProperty: 'delta',
            xLabel: 'Market Cap',
            yLabel: 'Volume',
            tickFormat: bigValueFormat,
            radius: 5,
            margins: {
                top: 20,
                right: 15,
                bottom: 30,
                left: 30
            },
            tooltip: ({uiFields: coin}) => `
                    <div class="title"}>
                      ${coin.name}</div>
                    <div>
                      <span class="caption">Price</span>
                      <span class="value">${coin.price}</span>
                    </div>
                    <div>
                      <span class="caption">Volume (24h)</span>
                      <span class="value">${coin.volume}</span>
                    </div>
                    <div>
                      <span class="caption">Market Cap</span>
                      <span class="value">${coin.marketCap}</span>
                    </div>
                    <div>
                      <span class="caption">Price Change</span>
                      <span class="value">${coin.delta}</span>
                    </div>`
        };
        this.scatterPlot = Object.create(ScatterPlot);
        this.scatterPlot.init(this.chart.current, chartOptions);
        this.scatterPlot.update(this.props.coins);
    }

    componentDidUpdate(prevProps) {
        const {coins} = this.props;
        if (prevProps.coins !== coins) {
            this.scatterPlot.update(coins);
        }
    }

    render() {
        return (
            <div className={styles.liquidityAnalysis}>
                <Header size='large'>
                    Liquidity Analysis
                </Header>

                <div className="chart-container" ref={this.chart}/>
            </div>
        );
    }
}

export default LiquidityAnalysis;