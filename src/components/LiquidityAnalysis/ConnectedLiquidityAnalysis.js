import {connect} from 'react-redux';
import {getSortedCoins} from '../../reducers/rootReducer';
import LiquidityAnalysis from './LiquidityAnalysis';

const mapStateToProps = state => ({
    coins: getSortedCoins(state)
});

export default connect(mapStateToProps)(LiquidityAnalysis);