import {connect} from 'react-redux';
import {getSortedCoins} from '../../reducers/rootReducer';
import MarketOverview from './MarketOverview';

const mapStateToProps = state => ({
    coins: getSortedCoins(state)
});

export default connect(mapStateToProps)(MarketOverview);