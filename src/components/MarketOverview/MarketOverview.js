import React from 'react';
import styles from './MarketOverview.module.css';
import {Table, Header} from 'semantic-ui-react';
import {Link} from 'react-router-dom';

const MarketOverview = (props) => (
    <div className={styles.marketOverview}>
        <Header size='large'>
            Market Overview
        </Header>

        <Table stackable selectable>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Rank</Table.HeaderCell>
                    <Table.HeaderCell>Name</Table.HeaderCell>
                    <Table.HeaderCell>Price</Table.HeaderCell>
                    <Table.HeaderCell>Price Change (24h)</Table.HeaderCell>
                    <Table.HeaderCell>Market Cap</Table.HeaderCell>
                    <Table.HeaderCell>Volume (24h)</Table.HeaderCell>
                </Table.Row>
            </Table.Header>

            <Table.Body>
                {props.coins.map(coin => coin.uiFields).map(coin => (
                    <Table.Row key={coin.id}>
                        <Table.Cell>{coin.rank}</Table.Cell>
                        <Table.Cell>
                            <Link to={`/coin/${coin.id}`}>
                                {coin.name}
                            </Link>
                        </Table.Cell>
                        <Table.Cell>{coin.price}</Table.Cell>
                        <Table.Cell
                            positive={coin.deltaPositive}
                            negative={coin.deltaNegative}>
                                {coin.delta}
                        </Table.Cell>
                        <Table.Cell>{coin.marketCap}</Table.Cell>
                        <Table.Cell>{coin.volume}</Table.Cell>
                    </Table.Row>
                ))}
            </Table.Body>
        </Table>
    </div>
);

export default MarketOverview;