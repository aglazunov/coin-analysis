import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'semantic-ui-css/semantic.min.css';
import Root from './app/Root';
import registerServiceWorker from './registerServiceWorker';

import configureStore from './app/configureStore';
const store = configureStore();

ReactDOM.render(<Root store={store}/>, document.getElementById('root'));
registerServiceWorker();
