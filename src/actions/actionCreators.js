import * as api from '../api/api';
import * as Types from './actionConstants';

const fetchAllCoinsBegin = () => ({
    type: Types.FETCH_ALL_COINS_BEGIN
});

const fetchAllCoinsSuccess = (coins) => ({
    type: Types.FETCH_ALL_COINS_SUCCESS,
    coins
});

const fetchAllCoinsFailure = (failure) => {
    return {
        type: Types.FETCH_ALL_COINS_FAILURE,
        failure,
        message: failure.message
    }
};

const fetchCoinBegin = () => ({
    type: Types.FETCH_COIN_BEGIN
});

const fetchCoinSuccess = (coin) => ({
    type: Types.FETCH_COIN_SUCCESS,
    coin
});

const fetchCoinFailure = (failure) => {
    return {
        type: Types.FETCH_COIN_FAILURE,
        failure,
        message: failure.message
    }
};

export const fetchTopCoins = (topCount) =>
    [
        fetchAllCoinsBegin(),
        api.fetchTopCoins(topCount)
            .then(result => fetchAllCoinsSuccess(result))
            .catch(result => fetchAllCoinsFailure(result))
    ];

export const fetchCoin = (id) =>
    [
        fetchCoinBegin(),
        api.fetchCoin(id)
            .then(result => fetchCoinSuccess(result))
            .catch(result => fetchCoinFailure(result))
    ];

export const changeTopCount = (count) => [
    {
        type: Types.CHANGE_TOP_COUNT,
        count
    },
    ...fetchTopCoins(count)
];