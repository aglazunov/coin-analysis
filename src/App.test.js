import React from 'react';
import configureStore from './app/configureStore';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Root from './app/Root';

Enzyme.configure({adapter: new Adapter()});

it('renders without crashing', () => {
    const store = configureStore();

    const wrapper = mount(<Root store={store}/>);
    wrapper.unmount();
});

it('renders an app and starts loading immediately', () => {
    const store = configureStore();
    const wrapper = mount(<Root store={store}/>);

    expect(wrapper.html()).toMatch(/Loading…/);
});
