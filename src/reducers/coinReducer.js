import * as actions from '../actions/actionConstants';
import {createReducer} from "../util/reducerUtils";

const initialState = [];

export default createReducer(initialState, {
    [actions.FETCH_COIN_BEGIN]: (state) => (null),
    [actions.FETCH_COIN_SUCCESS]: (state, {coin}) => coin
});