import * as actions from '../actions/actionConstants';
import {createReducer} from "../util/reducerUtils";
import * as Status from '../app/statuses';

const initialState = {
    message: null
};

const defaultFailureMessage = 'Failed to fetch coins';

export default createReducer(initialState, {
    [actions.FETCH_ALL_COINS_BEGIN]: (state) => ({
        message: 'Loading…',
        status: Status.LOADING
    }),
    [actions.FETCH_ALL_COINS_SUCCESS]: (state, {coins}) => ({
        status: Status.SUCCESS,
        message: null
    }),
    [actions.FETCH_ALL_COINS_FAILURE]: (state, {message}) => ({
        status: Status.FAILURE,
        message: typeof message === 'string' ? message : defaultFailureMessage
    })
});