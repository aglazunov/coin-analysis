import * as actions from '../actions/actionConstants';
import {createReducer} from "../util/reducerUtils";

const initialState = -1;

export default createReducer(initialState, {
    [actions.CHANGE_TOP_COUNT]: (state, {count}) => count
});