import {combineReducers} from 'redux';
import coinsReducer, * as coinsSelectors from './coinsReducer';
import coinReducer from './coinReducer';
import statusReducer from './statusReducer';
import topCountReducer from './topCountReducer';

export default combineReducers({
    coins: coinsReducer,
    topCount: topCountReducer,
    status: statusReducer,
    coin: coinReducer
});

export const getSortedCoins = state =>
    coinsSelectors.getSortedCoins(state.coins);

export const getCoin = state => state.coin;