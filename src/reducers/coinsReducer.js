import * as actions from '../actions/actionConstants';
import {createReducer} from "../util/reducerUtils";
import currencyFormatter from 'currency-formatter';

const initialState = [];

export default createReducer(initialState, {
    [actions.FETCH_ALL_COINS_BEGIN]: (state) => ([]),
    [actions.FETCH_ALL_COINS_SUCCESS]: (state, {coins}) => coins
});

const currency = (value, code, precision) => currencyFormatter.format(value, {
    code,
    precision
});

export const uiFields = coin => ({
    ...coin,
    delta: String(coin.delta) + '%',
    deltaPositive: coin.delta > 0,
    deltaNegative: coin.delta < 0,
    marketCap: currency(coin.marketCap, coin.currency, 0),
    price: currency(coin.price, coin.currency, 2),
    volume: currency(coin.volume, coin.currency, 0),
});

export const getSortedCoins = state => {
    let sortByRank = (a, b) => a.rank - b.rank;
    return state.slice().sort(sortByRank).map(coin => ({
        ...coin,
        uiFields: uiFields(coin)
    }));
};