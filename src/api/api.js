import {endpoint, currency} from './api-config';

export const handleErrors = (response) => {
    if (!response.ok) {
        throw new Error(response.statusText);
    }
    return response;
};

const getOverviewUrl = (limit) =>
    (typeof limit === 'number' && limit > 0) ?
        `${endpoint}/?limit=${limit}` :
        `${endpoint}/`;

const getCoinUrl = (id) =>
    `${endpoint}/${id}`;

const normalizeCoin = coin => {
    const quote = coin.quotes[currency];

    if (!(quote instanceof Object)) {
        return null;
    }

    const {id, rank, name} = coin,
        {
            price,
            percent_change_24h: delta,
            market_cap: marketCap,
            volume_24h: volume
        } = quote;

    return {
        id, rank, name, price, delta, marketCap, volume, currency
    }
};

export const fetchTopCoins = (limit) => fetch(getOverviewUrl(limit))
    .then(handleErrors)
    .then(response => response.json())
    .then(result => Object.values(result.data).map(normalizeCoin).filter(coin => coin instanceof Object));

export const fetchCoin = (id) => fetch(getCoinUrl(id))
    .then(handleErrors)
    .then(response => response.json())
    .then(response => response.data);