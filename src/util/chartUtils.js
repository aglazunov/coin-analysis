export const bigValueFormat = (number) => {
    let unitLetters = 'kMBTQ',
        unitExponent,
        unitMultiplier = 1000,
        unitLetter,
        resultValue;

    if (number < unitMultiplier) {
        resultValue = number;
        unitLetter = '';
    } else {
        // Which power of unit does bytes value contain
        unitExponent = Math.floor(Math.log(number) / Math.log(unitMultiplier));
        unitExponent = Math.min(unitExponent, unitLetters.length);
        unitLetter = unitLetters[unitExponent - 1];
        resultValue = Math.round(number / Math.pow(unitMultiplier, unitExponent));
    }

    // Form the result string
    resultValue += unitLetter;

    return resultValue;
};