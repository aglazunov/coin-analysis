# Solution for WATTx Front-End Engineer Challenge: **Top Coins**


## Problem

Create a webapp that allows for a simple analysis of crypto assets. The page should present a navigation bar with two 
pages: Market Overview with a table and Liquidity with a scatter plot chart. 

The app should fetch all the required data from the [coinmarketcap.com](https://coinmarketcap.com/api/) `/ticker` 
API endpoint. 

[See full task description here](https://github.com/WATTx/code-challenges/blob/master/frontend-engineer-challenge-top-coins.md)

## Demo

You can look at the solution in action at [coins.glazunov.eu](http://coins.glazunov.eu/).

## Running solution

run `yarn install` and then `yarn start`

## Solution parts

### State management

For application state management, `Redux` is being used. 

#### Store
Redux store is configured in a file `./app/configureStore.js`.
There, the middlewares for logging, Promises flow and batch dispatching are applied. 

#### Reducers
Reducers live in `reducers` folder. Root reducer combines three parts: coins, status and topCount reducers. topCount 
has -1 by default to signify no limit (or rather, a default api limit). There is a place in UI (dropdown) that knows 
about this _magic value_. `TODO` it should be refactored.  

`./util/reducerUtils` contains a `createReducer` method which allows to create a reducer from an action map, instead of 
writing one function with conditional logic.

#### Actions
Actions creators are defined in 
`./actions/actionCreators` file. The exported actions are fetchTopCoins and changeTopCount.
  
Batch dispatching pattern is used in data fetching scenario. When the fetchTopCoins action is created, it internally
dispatches fetchBegin action (it will be processed by status reducer to change status to loading) together with
async fetching data from the API, which will result in fetchSuccess in case of promise resolution or in fetchError
in case of its rejection.

#### API

`./api` contains function `fetchTopCoins` that does the API call to ticker API of coinmarketcap using fetch API and 
returns a promise that will resolve with array of entries, ready to use in UI (processed with `normalizeCoin` function).
`./api/config` allows specifying API endpoint and currency that's being looked up for price information. It will 
probably work only with USD, but the config is a neat place for this value. All the coins that don't have this currency
information, won't be returned by `fetchTopCoins` function.     

#### Selectors

Redux state selectors for coins are defined in `rootReducer` (to select a coins property) and in `coinsReducer` to get
the fields sorted by rank and ready to be consumed by the UI – the additional, processed properties (like formatted
currency fields) can be accessed by property `uiFields` of the coin. `TODO` fix: currently this is not fully refactored
and uiFields contains not only specific formatted properties but also original properties of the coin.

### Routing
React browser router is used. Because navigation header and currency selector are 'global' and are always displayed on 
top, the route components are added after them. App is wrapped in a function `withRouter` to be refreshed on the route
changes.

### Container and presentational components
`./app/App`, `./components/MarketOverview/MarketOverview` and `./components/LiquidityAnalysis/LiquidityAnalysis` are
presentational components. They all have _Connected…_ counterparts that have access redux store. 

## UI
Semantic react UI is being used. Table and scatter plot charts are responsive.

### Scatter plot chart
The chart is implemented with the help of d3 framework. The created object is `./components/ScatterPlot` and doesn't 
use react – react interacts with it by ref. It uses behavior delegation pattern and so is created with Object.create 
syntax. Three interesting solutions there:
1. Color of the dot signifies the price change (red for most fallen, green for the most risen) `TODO` even if they all 
have grown, the least grown will be red – fix it 
2. Large numbers are using k, M, B etc. labels. 
3. Exponential scales are used for better overview. 

## Further tasks (if only I have had more time… =)) 
- Fixing todos outlined in this doc
- Making styling more organized. Currently there are two empty styling modules for both page components. The other 
styles are disorganized.
- Tests for everything :)

Additional features
- Table sorting
- On the charts when points overlap, allow batch viewing (so even the bottom ones are accessible) 